Easy Credit
============

App nativa Android (Java) por Gustavo López Sánchez





Objetivos Técnicos
--------

Un cliente procura la implementación de la primera funcionalidad de su plataforma en de
préstamos: EasyCredit. Cualquier usuario es elegible para iniciar una nueva petición de
crédito, al hacer una petición de crédito, la plataforma deberá registrar ésta petición junto
con la información requerida de la misma, ésta solicitud deberá ser procesada por el
sistema mediante el siguiente criterio: personas de 20 años en adelante, que tengan
tarjeta de crédito.

Puntos importantes del proyecto
--------
 
 * Se desarrollo en torno al perfil de un Desarrollador Android, por lo que se optó por una
   configuración app nativa + Firebase para el manejo de persistencia y base de datos en la nube.
 * Se utilizó el servicio de FireStore (beta) como Base de datos en la nube.
 * El "procesamiento" se pide que se haga independiente al backend, en este caso se hace mediante la 
   programación de tareas en background en el móvil, se simulan 20 segundos de espera antes de tomarse la 
   desición si se acepta  o se rechaza la solicitud.
 * Se utilizaron ViewModels + LiveData, ademas de WorkManager (librerias Android JetPack).
 * FireStore sirve como persistencia local de datos y como base de datos en la nube.


Preparar el codigo fuente y compilar
--------------------

* El proyecto se desarrolló utilizando Android Studio 3.2.1 en Java con la ultima Versión de Gradle, 
  compilando para la api 28. (compileSdk).
* Clonar el proyecto, abrirlo en Android Studio y ejecturalo debería ser suficiente.
* No intentar correr el proyecto en modo Release, solo se hicieron pruebas en modo Debug.


Asunciones
--------------------
* No permitir hacer solicitudes a menores de 18 años ni mayores de 80.
* No permitir hacer solicitudes por cantidades menores de 100 ni mayores a 999,999,999.
* Se agregó un Splash Screen como presentación de la app.
* Se estipulan 3 modulos, pero es tan poca la informacion del usuario que decidí mostrar esa info
  en la Toolbar expandible.
* Los otros dos módulos los manejé como Tabs en un ViewPager
* El boton lo coloqué como FloatingActionButton y el formulario lo implementé como un Dialog.


Problemas al resolver éste ejercicio
--------------------
* El Timeout de FireStore (Firebase) es muy tardado, tengan paciencia si estan haciendo pruebas de 
  persistencia sin conexión.
  

Crítica constructiva sobre éste ejercicio
--------------------
Toda la redacción del problema fue ideado por una persona con un perfil WEB, estaría bien que hubiera
especificaciones basadas en las plataformas móviles.



Licencia
-------

    Copyright 2018 Gustavo López

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


