package com.glopez.easycredit;

import com.glopez.easycredit.data.model.User;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class UserModelTest {

    @Test
    public void userToMapIsOk(){

        final String username = "Gustavol95";
        User user = new User(username);

        Map<String, Object> map = user.toMap();

        Assert.assertNotNull(map);

        String userNameFromMap = map.get("username").toString();

        Assert.assertEquals(userNameFromMap,username);

    }

    @Test
    public void mapToUserIsOk(){

        Map<String, Object> map =new HashMap<>();

        map.put("username","Jorge");

        User user = User.parseUser(map);
        Assert.assertNotNull(user);

        Assert.assertEquals(user.getUsername(),"Jorge");


    }
}
