package com.glopez.easycredit.service;

import android.content.Context;

import com.glopez.easycredit.data.model.CreditApplication;
import com.glopez.easycredit.data.repository.CreditApplicationRepository;

import java.util.concurrent.LinkedBlockingQueue;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class ProcessApplicationWorker extends Worker {

    public ProcessApplicationWorker(Context context, WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        //Create object where well store the result
        final LinkedBlockingQueue<Result> result = new LinkedBlockingQueue<>();

        //Create Object from  Input Data
        CreditApplication creditApplication = CreditApplication.parseCreditApplication(getInputData().getKeyValueMap());

        //Instantiate repository
        CreditApplicationRepository creditApplicationRepository = new CreditApplicationRepository();

        //Set Status based on business rules
        creditApplication.setStatus(
                creditApplicationRepository.processApplication(creditApplication)
        );

        //Update the object in Database and Cloud Database
        creditApplicationRepository.createCreditApplication(creditApplication, new CreditApplicationRepository.OnApplicationSave() {
            @Override
            public void success(CreditApplication creditApplication) {
                try {
                    result.put(Result.SUCCESS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(Exception e) {
                try {
                    result.put(Result.RETRY);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }, true);
        try {
            return result.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return Result.RETRY;
        }
    }
}
