package com.glopez.easycredit.application;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.glopez.easycredit.utils.PreferencesHelper;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

import io.fabric.sdk.android.Fabric;

public class App extends Application {

    private static App instance;
    private FirebaseFirestore db;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Fabric.with(this, new Crashlytics());
        //Instantiate FireCloud Database  with persistence setting
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);


        //Instantiate Preferences Helper
        PreferencesHelper.getInstance().setContext(this);
    }


    public static synchronized App getInstance(){
        return instance;
    }

    public FirebaseFirestore getDb() {
        return db;
    }
}
