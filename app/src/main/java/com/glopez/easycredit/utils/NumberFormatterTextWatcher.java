package com.glopez.easycredit.utils;


import android.text.Editable;
import android.text.TextWatcher;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import androidx.appcompat.widget.AppCompatEditText;

/**
 *  Formats input number from editText
 *  calculates amount with applied fee
 *  Divides fee amount by number of installments
 *  Formats all 3 number mentioned before
 *
 *  used in @link {@link com.glopez.easycredit.ui.main.NewApplicationDialogFragment}
 */
public class NumberFormatterTextWatcher implements TextWatcher {


    public interface INumberFormatter{
        void onAmountPerInstallmentChange(String amount);
        void onTotalAmountChange(String totalAmount);
    }

    private AppCompatEditText editText;
    private double feeFactor = 1.05;
    private int numberOfInstallments = 3;
    private INumberFormatter listener;
    private double valueWithFee = 0;
    private double partialValue = 0;
    private long value = 0;
    private   DecimalFormat formatter;
    private   DecimalFormat decimalFormatter;

    public NumberFormatterTextWatcher(AppCompatEditText editText, INumberFormatter listener) {
        this.editText = editText;
        this.listener = listener;

        //Instantiate Formatter
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###,###,###");
        decimalFormatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        decimalFormatter.applyPattern("#,###,###,##0.00");
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        editText.removeTextChangedListener(this);

        try {
            String originalString = editable.toString();

            if (originalString.contains("$")) {
                originalString = originalString.replaceAll("\\$", "");
            }


            if (originalString.contains(",")) {
                originalString = originalString.replaceAll(",", "");
            }

            //Obtain unformatted amount
             value = Long.parseLong(originalString);

            //Obtain unformatted amount with fee added
             valueWithFee = value *feeFactor;

            //Obtain unformatted value per installment
              partialValue = Double.parseDouble(originalString)/numberOfInstallments;



            //Format all values
            String formattedValue = formatter.format(value);
            String formattedValueWithFee = decimalFormatter.format(valueWithFee);
            String formattedPartialValue = decimalFormatter.format(partialValue);

            //setting text after format to EditText
            editText.setText("$"+formattedValue);
            editText.setSelection(editText.getText().length());

            //Notify via listener the other formatted values
            listener.onAmountPerInstallmentChange(formattedPartialValue);
            listener.onTotalAmountChange(formattedValueWithFee);


        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }

        editText.addTextChangedListener(this);
    }

    public AppCompatEditText getEditText() {
        return editText;
    }

    public void setEditText(AppCompatEditText editText) {
        this.editText = editText;
    }


    public void setNewPaymentOption(double feeFactor, int numberOfInstallments) {
        this.feeFactor = feeFactor;
        this.numberOfInstallments = numberOfInstallments;
        afterTextChanged(editText.getText());
    }


    public long getValue() {
        return value;
    }

    public double getValueWithFee() {
        return valueWithFee;
    }

    public double getPartialValue() {
        return partialValue;
    }
}
