package com.glopez.easycredit.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesHelper {

    private static PreferencesHelper instance;
    private static final String USERNAME = "username";
    private static final String SHARE_PREFERENCES_FILE = "easy-credit-prefs";
    private SharedPreferences sharedPreferences;

    private PreferencesHelper() {
    }



    public static synchronized PreferencesHelper getInstance() {
        if (instance == null) {
            instance = new PreferencesHelper();
        }
        return instance;
    }

    public void setContext(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARE_PREFERENCES_FILE, 0);
    }


    public void saveUsername(String username) {
        if(username!=null){
            sharedPreferences.edit()
                    .putString(USERNAME, username)
                    .apply();
        }else{
            sharedPreferences.edit().remove(USERNAME)
                    .apply();
        }

    }

    public String getUserName() {
        return sharedPreferences
                .getString(USERNAME,null);
    }
}
