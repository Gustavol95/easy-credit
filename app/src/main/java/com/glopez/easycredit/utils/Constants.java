package com.glopez.easycredit.utils;

public class Constants {

    public static final int LOGGED_IN = 1;
    public static final int NO_SESSION = 2;

    public static final String USER_COLLECTION = "users";
    public static final String CREDIT_COLLECTION = "credits";
}
