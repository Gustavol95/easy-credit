package com.glopez.easycredit.data.model;

import com.glopez.easycredit.utils.PreferencesHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CreditApplication {


    // FireBase Cloud Constants
    public static final String AGE_KEY = "age";
    public static final String AMOUNT_KEY = "amount";
    public static final String AMOUNT_WITH_INTEREST_KEY = "amountWithInterest";
    public static final String HAS_CREDITCARD_KEY = "hasCreditCard";
    public static final String NUMBER_OF_INSTALLMENTS_KEY = "numberOfInstallents";
    public static final String FEE_KEY = "fee";
    public static final String STATUS_KEY = "status";
    public static final String USERNAME_KEY = "username";
    public static final String UUID_KEY = "uuid";

    public static final String PENDING = "pending";
    public static final String ACCEPTED = "accepted";
    public static final String REJECTED = "rejected";


    private int age;
    private long amount;
    private double amountWithInterest;
    private boolean hasCreditCard;
    private int numberOfInstallments;
    private double fee;
    private String status;
    private String username;
    private String uuid;

    public CreditApplication() {
        this.age = 0;
        this.amount = 0L;
        this.amountWithInterest = 0;
        this.fee = 1.05;
        this.hasCreditCard = false;
        this.numberOfInstallments = 3;
        this.status = PENDING;
        this.username = PreferencesHelper.getInstance().getUserName();
        this.uuid = UUID.randomUUID().toString();
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(AGE_KEY, age);
        map.put(AMOUNT_KEY, amount);
        map.put(AMOUNT_WITH_INTEREST_KEY, amountWithInterest);
        map.put(HAS_CREDITCARD_KEY, hasCreditCard);
        map.put(NUMBER_OF_INSTALLMENTS_KEY, numberOfInstallments);
        map.put(FEE_KEY, fee);
        map.put(STATUS_KEY, status);
        map.put(USERNAME_KEY, username);
        map.put(UUID_KEY, uuid);
        return map;
    }

    public static Map<String, Object> toMap(CreditApplication creditApplication) {
        if (creditApplication != null) {
            Map<String, Object> map = new HashMap<>();
            map.put(AGE_KEY, creditApplication.getAge());
            map.put(AMOUNT_KEY, creditApplication.getAmount());
            map.put(AMOUNT_WITH_INTEREST_KEY, creditApplication.getAmountWithInterest());
            map.put(HAS_CREDITCARD_KEY, creditApplication.isHasCreditCard());
            map.put(NUMBER_OF_INSTALLMENTS_KEY, creditApplication.getNumberOfInstallments());
            map.put(FEE_KEY, creditApplication.getFee());
            map.put(STATUS_KEY, creditApplication.getStatus());
            map.put(USERNAME_KEY, creditApplication.getUsername());
            map.put(UUID_KEY, creditApplication.getUuid());
            return map;
        } else
            return null;

    }


    public static CreditApplication parseCreditApplication(Map<String, Object> creditAppMap) {
        if (creditAppMap != null) {
            CreditApplication creditApplication = new CreditApplication();
            creditApplication.setAge(Integer.parseInt( creditAppMap.get(AGE_KEY).toString()));
            creditApplication.setAmount((long) creditAppMap.get(AMOUNT_KEY));
            creditApplication.setAmountWithInterest((double) creditAppMap.get(AMOUNT_WITH_INTEREST_KEY));
            creditApplication.setHasCreditCard((boolean) creditAppMap.get(HAS_CREDITCARD_KEY));
            creditApplication.setNumberOfInstallments(Integer.parseInt(  creditAppMap.get(NUMBER_OF_INSTALLMENTS_KEY).toString()));
            creditApplication.setFee((double) creditAppMap.get(FEE_KEY));
            creditApplication.setStatus(creditAppMap.get(STATUS_KEY).toString());
            creditApplication.setUsername(creditAppMap.get(USERNAME_KEY).toString());
            creditApplication.setUuid(creditAppMap.get(UUID_KEY).toString());
            return creditApplication;
        } else
            return null;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public double getAmountWithInterest() {
        return amountWithInterest;
    }

    public void setAmountWithInterest(double amountWithInterest) {
        this.amountWithInterest = amountWithInterest;
    }

    public boolean isHasCreditCard() {
        return hasCreditCard;
    }

    public void setHasCreditCard(boolean hasCreditCard) {
        this.hasCreditCard = hasCreditCard;
    }

    public int getNumberOfInstallments() {
        return numberOfInstallments;
    }

    public void setNumberOfInstallments(int numberOfInstallments) {
        this.numberOfInstallments = numberOfInstallments;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "CreditApplication{" +
                "age=" + age +
                ", amount=" + amount +
                ", amountWithInterest=" + amountWithInterest +
                ", hasCreditCard=" + hasCreditCard +
                ", numberOfInstallments=" + numberOfInstallments +
                ", fee=" + fee +
                ", status='" + status + '\'' +
                ", username='" + username + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }


}