package com.glopez.easycredit.data.repository;


import com.glopez.easycredit.application.App;
import com.glopez.easycredit.data.model.User;
import com.glopez.easycredit.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;

import androidx.annotation.NonNull;


public class UserRepository {

    public interface OnUserLogin {
        void success(User user);

        void failure(Exception e);
    }

    /**
     *  Queries for a user that matches the given parameter in cloud database,
     *  if not, then a new user is created and returned.
     * @param username Username used to query the database
     * @param listener interface to communicate result
     */
    public void getUserByUsername(final String username, final OnUserLogin listener) {
        DocumentReference docRef = App.getInstance()
                .getDb()
                .collection(Constants.USER_COLLECTION)
                .document(username);

        docRef.get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                //Map to User Object
                                User user = User.parseUser(document.getData());
                                if (user != null)
                                    listener.success(user);

                            } else {
                               //Create the user
                                createUser(username,listener);
                            }
                        } else {
                            //Query Failed
                            listener.failure(task.getException());
                        }
                    }
                });
    }

    /**
     *  Creates new user in Database and Cloud Database.
     * @param username Username to be registered
     * @param listener Interface to communicate success or failure.
     */
    private void createUser(String username, final OnUserLogin listener){
       //Create a User Object
        final User newUser = new User(username);


        App.getInstance().getDb().collection(Constants.USER_COLLECTION)
                .document(newUser.getUsername())
                .set(newUser.toMap())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                       listener.success(newUser);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                       listener.failure(e);
                    }
                });
    }
}
