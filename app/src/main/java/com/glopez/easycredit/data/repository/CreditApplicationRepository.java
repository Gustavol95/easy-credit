package com.glopez.easycredit.data.repository;


import com.glopez.easycredit.application.App;
import com.glopez.easycredit.data.mapper.CreditApplicationMapper;
import com.glopez.easycredit.data.model.CreditApplication;
import com.glopez.easycredit.service.ProcessApplicationWorker;
import com.glopez.easycredit.utils.Constants;
import com.glopez.easycredit.utils.PreferencesHelper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

public class CreditApplicationRepository {

    public interface OnApplicationSave {
        void success(CreditApplication creditApplication);

        void failure(Exception e);
    }

    /**
     * saves a CreditApplication Object in database and syncs with cloud database
     * @param creditApplication CreditApplication Object
     * @param listener Interface to communicate succes or error
     */
    public void createCreditApplication(final CreditApplication creditApplication, final OnApplicationSave listener, final boolean update){
        App.getInstance().getDb().collection(Constants.CREDIT_COLLECTION)
                .document(creditApplication.getUuid())
                .set(creditApplication.toMap())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        listener.success(creditApplication);
                        if(!update)
                            scheduleProcessingWorker(creditApplication);


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        listener.failure(e);
                    }
                });
    }


    public interface OnApplicationsInProccess{
        void onApplicationsInProccessChanged(List<CreditApplication> list);
        void onError(Exception e);
    }

    public void getApplicationsInProccess(final OnApplicationsInProccess listener){
        // Create a reference to the cities collection
        CollectionReference citiesRef =   App.getInstance().getDb().collection(Constants.CREDIT_COLLECTION);

        // Create a query against the collection.
        final Query query = citiesRef.whereEqualTo(CreditApplication.USERNAME_KEY, PreferencesHelper.getInstance().getUserName())
                                .whereEqualTo(CreditApplication.STATUS_KEY, CreditApplication.PENDING);

        final CreditApplicationMapper creditApplicationMapper = new CreditApplicationMapper();

        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(queryDocumentSnapshots != null)
                   listener.onApplicationsInProccessChanged(
                           creditApplicationMapper.mapSnapshot(queryDocumentSnapshots));

                if(e != null)
                    listener.onError(e);

            }
        });
    }

    public interface OnProccessedApplication{
        void onProccessedApplicationChanged(List<CreditApplication> list);
        void onError(Exception e);
    }

    public void getProccessedApplications(final OnProccessedApplication listener){
        // Create a reference to the cities collection
        CollectionReference citiesRef =   App.getInstance().getDb().collection(Constants.CREDIT_COLLECTION);

        // Create a query against the collection.
        Query query = citiesRef.whereEqualTo(CreditApplication.USERNAME_KEY, PreferencesHelper.getInstance().getUserName());

        final CreditApplicationMapper creditApplicationMapper = new CreditApplicationMapper();

        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(queryDocumentSnapshots != null)
                    listener.onProccessedApplicationChanged(
                            creditApplicationMapper.mapSnapshotIgnorePending(queryDocumentSnapshots));

                if(e != null)
                    listener.onError(e);
            }
        });
    }

    /**
     * Schedules in 20 seconds a process that will accept or reject the application
     * @param creditApplication CreditApplication object
     */

    private void scheduleProcessingWorker(CreditApplication creditApplication){
        // Create the Data object:
        Data myData = new Data.Builder()
                .putAll(creditApplication.toMap())
                .build();

        //Create constraints
        Constraints constraints =  new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();
        //build request
        OneTimeWorkRequest syncWorker =
                new OneTimeWorkRequest.Builder(ProcessApplicationWorker.class)
                        .setConstraints(constraints)
                        .setInputData(myData)
                        .setInitialDelay(20, TimeUnit.SECONDS)
                        .build();
        //queue request
        WorkManager.getInstance().enqueue(syncWorker);

    }

    public String processApplication(CreditApplication creditApplication){
        if(creditApplication.getAge()>= 20 && creditApplication.isHasCreditCard())
            return CreditApplication.ACCEPTED;
        else
            return CreditApplication.REJECTED;
    }
}
