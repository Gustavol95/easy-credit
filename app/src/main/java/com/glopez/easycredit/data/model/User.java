package com.glopez.easycredit.data.model;

import java.util.HashMap;
import java.util.Map;

public class User {

    // FireBase Cloud Constants
    private static final String USERNAME_KEY = "username";
    private String username;

    public User(String username) {
        this.username = username;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                '}';
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(USERNAME_KEY, username);
        return map;
    }

    public static Map<String, Object> toMap(User user) {
        if (user != null) {
            Map<String, Object> map = new HashMap<>();
            map.put(USERNAME_KEY, user.getUsername());
            return map;
        } else
            return null;

    }


    public static User parseUser(Map<String, Object> userMap) {
        if (userMap != null) {
            Object username = userMap.get(USERNAME_KEY);
            if (username != null)
                return new User(username.toString());
            else return null;
        } else
            return null;
    }
}
