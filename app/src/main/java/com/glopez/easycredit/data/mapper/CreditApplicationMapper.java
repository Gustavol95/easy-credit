package com.glopez.easycredit.data.mapper;


import com.glopez.easycredit.data.model.CreditApplication;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class CreditApplicationMapper {

    private boolean isMapping = false;

    public List<CreditApplication> mapSnapshot(QuerySnapshot queryDocumentSnapshots) {
        if (!isMapping) {
            isMapping = true;
            List<CreditApplication> creditApplications = new ArrayList<>();

            for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                creditApplications.add(CreditApplication.parseCreditApplication(snapshot.getData()));
            }

            isMapping = false;
            return creditApplications;
        } else return null;

    }


    public List<CreditApplication> mapSnapshotIgnorePending(QuerySnapshot queryDocumentSnapshots) {
        if (!isMapping) {
            isMapping = true;
            List<CreditApplication> creditApplications = new ArrayList<>();

            for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                CreditApplication creditApplication = CreditApplication.parseCreditApplication(snapshot.getData());

                if(!creditApplication.getStatus().equals(CreditApplication.PENDING))
                    creditApplications.add(creditApplication);
            }

            isMapping = false;
            return creditApplications;
        } else return null;

    }

}
