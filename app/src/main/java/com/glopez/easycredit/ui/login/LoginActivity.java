package com.glopez.easycredit.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.glopez.easycredit.R;
import com.glopez.easycredit.data.model.User;
import com.glopez.easycredit.ui.main.MainActivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.editLogin)
    AppCompatEditText editLogin;

    @BindView(R.id.buttonLogin)
    AppCompatButton buttonLogin;

    @BindView(R.id.frameLoading)
    FrameLayout frameLoading;

    @BindView(R.id.tvLoading)
    TextView tvLoading;

    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Bind Views
        ButterKnife.bind(this);
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        attachObservers();
    }

    /**
     * Observes all LiveData objets related to the activity
     */
    private void attachObservers() {
        loginViewModel.getIsLoading().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String loadingText) {
                if (loadingText != null) {
                    disableViews(true);
                    frameLoading.setVisibility(View.VISIBLE);
                    tvLoading.setText(loadingText);

                } else {
                    disableViews(false);
                    frameLoading.setVisibility(View.GONE);
                }
            }
        });

        loginViewModel.getLoginSuccess().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if (user != null) {
                    Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(mainIntent);
                    finish();
                }
            }
        });

        loginViewModel.getLoginFailure().observe(this, new Observer<Throwable>() {
            @Override
            public void onChanged(Throwable throwable) {
                if (throwable != null)
                    Toast.makeText(LoginActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Disables/Enables all views while screen is loading
     *
     * @param disable disable flag, use false to enable again
     */
    public void disableViews(boolean disable) {
        buttonLogin.setEnabled(!disable);
        editLogin.setEnabled(!disable);
    }

    @OnClick(R.id.buttonLogin)
    public void onButtonLoginClick() {
        loginViewModel.login(editLogin.getText());
    }
}
