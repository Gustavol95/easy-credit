package com.glopez.easycredit.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.glopez.easycredit.R;
import com.glopez.easycredit.data.model.CreditApplication;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CreditAppListFragment extends Fragment {

    static final int TYPE_HYSTORY = 1;
    static final int TYPE_PENDING = 2;
    private static final String TYPE = "type";

    @BindView(R.id.recycler)
    RecyclerView recycler;

    @BindView(R.id.tvEmpty)
    TextView tvEmpty;

    private CreditAppAdapter adapter;
    private List<CreditApplication> creditApplications = new ArrayList<>();
    private int type;
    private MainViewModel mainViewModel;


    static CreditAppListFragment newInstance(int fragmentType) {
        CreditAppListFragment creditAppListFragment = new CreditAppListFragment();
        creditAppListFragment.setType(fragmentType);
        return creditAppListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_credit_app_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null)
            type = savedInstanceState.getInt(TYPE);

        adapter = new CreditAppAdapter();
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler.setAdapter(adapter);
        mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        attachToObservers();
    }

    /**
     * Observes live data objects depending on type set
     */
    private void attachToObservers() {
        if (type == TYPE_PENDING)
            mainViewModel.getApplicationsOnProccess().observe(this, new Observer<List<CreditApplication>>() {
                @Override
                public void onChanged(List<CreditApplication> creditApplications) {
                    if (creditApplications != null) {
                        if (creditApplications.size() != 0) {
                            recycler.setVisibility(View.VISIBLE);
                            tvEmpty.setVisibility(View.GONE);
                            adapter.setCreditApplications(creditApplications);
                        } else {
                            recycler.setVisibility(View.GONE);
                            tvEmpty.setVisibility(View.VISIBLE);

                        }
                    }
                }
            });

        if (type == TYPE_HYSTORY)
            mainViewModel.getProccessedApplications().observe(this, new Observer<List<CreditApplication>>() {
                @Override
                public void onChanged(List<CreditApplication> creditApplications) {
                    if (creditApplications != null) {
                        if (creditApplications.size() != 0) {
                            recycler.setVisibility(View.VISIBLE);
                            tvEmpty.setVisibility(View.GONE);
                            adapter.setCreditApplications(creditApplications);
                        } else {
                            recycler.setVisibility(View.GONE);
                            tvEmpty.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(TYPE, type);
    }
}
