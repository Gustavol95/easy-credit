package com.glopez.easycredit.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.glopez.easycredit.R;
import com.glopez.easycredit.ui.login.LoginActivity;
import com.glopez.easycredit.ui.main.MainActivity;
import com.glopez.easycredit.utils.Constants;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class SplashActivity extends AppCompatActivity {

    private SplashViewModel splashViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.activity_splash);
        splashViewModel = ViewModelProviders.of(this).get(SplashViewModel.class);
        attachObservers();

    }


    /**
     *  Observes all LiveData objets related to the activity
     */
    private void attachObservers(){
        splashViewModel.getStartSplashObservable().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(integer!=null){
                 switch (integer){
                     case Constants.NO_SESSION:
                         Intent loginIntent =
                                 new Intent(SplashActivity.this,LoginActivity.class);
                         startActivity(loginIntent);
                         finish();
                         break;
                     case Constants.LOGGED_IN:
                         Intent mainIntent =
                                 new Intent(SplashActivity.this,MainActivity.class);
                         startActivity(mainIntent);
                         finish();
                         break;
                 }
                }
            }
        });
    }
}
