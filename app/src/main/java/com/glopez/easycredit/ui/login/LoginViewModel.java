package com.glopez.easycredit.ui.login;

import android.text.Editable;

import com.glopez.easycredit.data.model.User;
import com.glopez.easycredit.data.repository.UserRepository;
import com.glopez.easycredit.utils.PreferencesHelper;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class LoginViewModel extends ViewModel {

    private UserRepository userRepository = new UserRepository();

    private final MutableLiveData<String> isLoading = new MutableLiveData<>();
    private final MutableLiveData<User> loginSuccess = new MutableLiveData<>();
    private final MutableLiveData<Throwable> loginFailure = new MutableLiveData<>();


    /**
     * Asks the userRepository to find user in database and return it
     * @param username username to find
     */
    void login(Editable username) {
        if (validateInput(username)) {
            isLoading.setValue("Iniciando Sesión");
            String lowerCaseUsername = username.toString().trim().toLowerCase();
            userRepository.getUserByUsername(lowerCaseUsername, new UserRepository.OnUserLogin() {
                @Override
                public void success(User user) {
                    isLoading.setValue(null);
                    loginSuccess.setValue(user);
                    // Save logged in user reference
                    PreferencesHelper.getInstance().saveUsername(user.getUsername());
                }

                @Override
                public void failure(Exception e) {
                    isLoading.setValue(null);
                    loginFailure.setValue(e);
                }

            });
        } else {
            loginFailure.postValue(new Exception("Usuario Inválido."));
        }
    }

    /**
     *  Validation method to avoid usernames with less than 3 characters long
     * @param editable editable from login Activity EditText
     * @return true if everything's ok
     */
    private boolean validateInput(Editable editable) {
        if (editable == null)
            return false;

        return editable.toString().trim().length() >= 3;

    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }

    MutableLiveData<String> getIsLoading() {
        return isLoading;
    }

    MutableLiveData<User> getLoginSuccess() {
        return loginSuccess;
    }

    MutableLiveData<Throwable> getLoginFailure() {
        return loginFailure;
    }
}
