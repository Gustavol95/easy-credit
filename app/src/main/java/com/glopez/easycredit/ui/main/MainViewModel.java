package com.glopez.easycredit.ui.main;


import com.glopez.easycredit.data.model.CreditApplication;
import com.glopez.easycredit.data.repository.CreditApplicationRepository;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

class MainViewModel extends ViewModel {


    private final MutableLiveData<String> validationErrorMessage = new MutableLiveData<>();
    private MutableLiveData<List<CreditApplication>> applicationsOnProccess;
    private MutableLiveData<List<CreditApplication>> proccessedApplications;
    private final MutableLiveData<String> requestFailure = new MutableLiveData<>();
    private final MutableLiveData<Boolean> validationSuccess = new MutableLiveData<>();
    private final MutableLiveData<String> isLoading = new MutableLiveData<>();


    private final CreditApplicationRepository creditApplicationRepository = new CreditApplicationRepository();


    /**
     * validates and saves a CreditApplication in Database and cloud database
     *
     * @param creditApplication CreditApplication object to save
     */
    void saveCreditApplication(CreditApplication creditApplication) {
        if (validateCreditApp(creditApplication)) {

            isLoading.postValue("Enviando Información");
            validationSuccess.postValue(true);
            creditApplicationRepository.createCreditApplication(
                    creditApplication, new CreditApplicationRepository.OnApplicationSave() {
                        @Override
                        public void success(CreditApplication creditApplication) {
                            isLoading.postValue(null);
                        }

                        @Override
                        public void failure(Exception e) {
                            isLoading.postValue(null);
                            requestFailure.postValue(e.getMessage());
                        }
                    }, false);
        }

    }


    /**
     * Validates all business rules for CreditApplication are ok
     *
     * @param creditApplication creditApp Object to validate
     */
    private boolean validateCreditApp(CreditApplication creditApplication) {

        if (creditApplication.getAge() < 18 || creditApplication.getAge() > 80) {
            validationErrorMessage.postValue("Edad inválida");
            return false;
        }

        if (creditApplication.getAmount() < 100) {
            validationErrorMessage.postValue("Cantidad muy baja, mínimo $100.00");
            return false;
        }


        if (creditApplication.getAmountWithInterest() == 0) {
            validationErrorMessage.postValue("Hubo un error al calcular los intereses");
            return false;
        }
        return true;
    }

    private void getAppsInProccess() {
        creditApplicationRepository.getApplicationsInProccess(new CreditApplicationRepository.OnApplicationsInProccess() {
            @Override
            public void onApplicationsInProccessChanged(List<CreditApplication> list) {
                applicationsOnProccess.postValue(list);
            }

            @Override
            public void onError(Exception e) {
                requestFailure.postValue(e.getMessage());
            }
        });
    }

    private void getProccesedApps() {
        creditApplicationRepository.getProccessedApplications(new CreditApplicationRepository.OnProccessedApplication() {
            @Override
            public void onProccessedApplicationChanged(List<CreditApplication> list) {
                proccessedApplications.postValue(list);
            }

            @Override
            public void onError(Exception e) {
                requestFailure.postValue(e.getMessage());

            }
        });
    }

    MutableLiveData<String> getValidationErrorMessage() {
        return validationErrorMessage;
    }

    MutableLiveData<String> getRequestFailure() {
        return requestFailure;
    }

    MutableLiveData<String> getIsLoading() {
        return isLoading;
    }

    MutableLiveData<Boolean> getValidationSuccess() {
        return validationSuccess;
    }

    MutableLiveData<List<CreditApplication>> getApplicationsOnProccess() {
        if (applicationsOnProccess == null) {
            applicationsOnProccess = new MutableLiveData<>();
            getAppsInProccess();
            return applicationsOnProccess;
        } else
            return applicationsOnProccess;
    }

    MutableLiveData<List<CreditApplication>> getProccessedApplications() {
        if (proccessedApplications == null) {
            proccessedApplications = new MutableLiveData<>();
            getProccesedApps();
            return proccessedApplications;
        } else
            return proccessedApplications;
    }

}
