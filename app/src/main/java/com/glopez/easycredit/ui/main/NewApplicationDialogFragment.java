package com.glopez.easycredit.ui.main;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.glopez.easycredit.R;
import com.glopez.easycredit.data.model.CreditApplication;
import com.glopez.easycredit.utils.NumberFormatterTextWatcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewApplicationDialogFragment extends DialogFragment {


    @BindView(R.id.editAge)
    AppCompatEditText editAge;

    @BindView(R.id.editAmount)
    AppCompatEditText editAmount;

    @BindView(R.id.checkHasCreditCard)
    AppCompatCheckBox checkHasCreditCard;

    @BindView(R.id.tvFee)
    TextView tvFee;

    @BindView(R.id.tvNumberOfInstallments)
    TextView tvNumberOfInstallments;

    @BindView(R.id.tvAmountPerInstallment)
    TextView tvAmountPerInstallment;

    @BindView(R.id.tvTotalAmount)
    TextView tvTotalAmount;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvContinue)
    TextView tvContinue;

    private NumberFormatterTextWatcher numberFormatterTextWatcher;

    private MainViewModel mainViewModel;

    private CreditApplication creditApplication;

    Context context;


    static NewApplicationDialogFragment newInstance() {
        return new NewApplicationDialogFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_fragment_new_application, container, false);
        ButterKnife.bind(this, v);
        setCancelable(false);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        creditApplication = new CreditApplication();
        numberFormatterTextWatcher = new NumberFormatterTextWatcher(editAmount, new NumberFormatterTextWatcher.INumberFormatter() {
            @Override
            public void onAmountPerInstallmentChange(String amount) {
                tvAmountPerInstallment.setText(" de $" + amount);
                //update model
                creditApplication.setAmount(numberFormatterTextWatcher.getValue());
                creditApplication.setAmountWithInterest(numberFormatterTextWatcher.getValueWithFee());
            }

            @Override
            public void onTotalAmountChange(String totalAmount) {
                tvTotalAmount.setText("Total $" + totalAmount);
                //update model
                creditApplication.setAmount(numberFormatterTextWatcher.getValue());
                creditApplication.setAmountWithInterest(numberFormatterTextWatcher.getValueWithFee());
            }
        });
        editAmount.addTextChangedListener(numberFormatterTextWatcher);

        checkHasCreditCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //update model
                creditApplication.setHasCreditCard(isChecked);
            }
        });

        //Set default values
        editAmount.setText("0");
        numberFormatterTextWatcher.setNewPaymentOption(1.05, 3);
        tvNumberOfInstallments.setText(R.string.new_app_frag_3Payments);
        tvFee.setText(R.string.new_app_frag_3Payments_fee_warning);


        mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        //Start observing liveData Objects
        attachToObservers();
    }

    private void attachToObservers() {
        mainViewModel.getValidationErrorMessage().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String validationErrorMsg) {
                if (validationErrorMsg != null){
                    if(context != null)
                        Toast.makeText(context, validationErrorMsg, Toast.LENGTH_LONG).show();
                    mainViewModel.getValidationErrorMessage().postValue(null);
                }
            }
        });

    }

    @OnClick({R.id.radio1, R.id.radio2, R.id.radio3})
    public void OnRadioClick(RadioButton radioButton) {
        switch (radioButton.getId()) {
            case R.id.radio1:
                numberFormatterTextWatcher.setNewPaymentOption(1.05, 3);
                tvNumberOfInstallments.setText(R.string.new_app_frag_3Payments);
                tvFee.setText(R.string.new_app_frag_3Payments_fee_warning);
                //Update Model
                creditApplication.setFee(1.05);
                creditApplication.setNumberOfInstallments(3);
                break;
            case R.id.radio2:
                numberFormatterTextWatcher.setNewPaymentOption(1.07, 6);
                tvNumberOfInstallments.setText(R.string.new_app_frag_6Payments);
                tvFee.setText(R.string.new_app_frag_6Payments_fee_warning);
                //Update Model
                creditApplication.setFee(1.07);
                creditApplication.setNumberOfInstallments(6);
                break;
            case R.id.radio3:
                numberFormatterTextWatcher.setNewPaymentOption(1.12, 9);
                tvNumberOfInstallments.setText(R.string.new_app_frag_9Payments);
                tvFee.setText(R.string.new_app_frag_9Payments_fee_warning);

                //Update Model
                creditApplication.setFee(1.12);
                creditApplication.setNumberOfInstallments(9);
                break;
        }
    }

    @OnClick(R.id.tvContinue)
    public void onClickTvContinue() {
        int age;
        try {
            age = Integer.parseInt(editAge.getText().toString());

        } catch (NumberFormatException e) {
            age = 0;
        }
        creditApplication.setAge(age);
        mainViewModel.saveCreditApplication(creditApplication);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        if (dialog.getWindow() != null)
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }


    @OnClick(R.id.tvCancel)
    public void onClickTvCancel() {
        dismiss();
    }




}