package com.glopez.easycredit.ui.main;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class MainActivityPagerAdapter extends FragmentPagerAdapter {

    private final List<CreditAppListFragment> mFragmentList  = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public MainActivityPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(CreditAppListFragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    public List<CreditAppListFragment> getmFragmentList() {
        return mFragmentList;
    }
}
