package com.glopez.easycredit.ui.main;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.glopez.easycredit.R;
import com.glopez.easycredit.data.model.CreditApplication;
import com.glopez.easycredit.ui.login.LoginActivity;
import com.glopez.easycredit.utils.PreferencesHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fabNewApplication)
    FloatingActionButton fabNewApplication;

    @BindView(R.id.frameLoading)
    FrameLayout frameLoading;

    @BindView(R.id.tvLoading)
    TextView tvLoading;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.tvHeaderPendingApps)
    TextView tvHeaderPendingApps;

    @BindView(R.id.tvHeaderUsername)
    TextView tvHeaderUsername;

    MainActivityPagerAdapter pagerAdapter;

    private MainViewModel mainViewModel;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(R.string.app_name);

        tvHeaderUsername.setText("Bienvenido(a) "+PreferencesHelper.getInstance().getUserName());

        pagerAdapter = new MainActivityPagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFrag(CreditAppListFragment.newInstance(CreditAppListFragment.TYPE_HYSTORY), "Historial");
        pagerAdapter.addFrag(CreditAppListFragment.newInstance(CreditAppListFragment.TYPE_PENDING), "Solicitudes");

        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        attachObservers();
    }

    private void attachObservers() {
        mainViewModel.getIsLoading().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String loadingText) {
                if (loadingText != null) {
                    //  disableViews(true);
                    frameLoading.setVisibility(View.VISIBLE);
                    tvLoading.setText(loadingText);

                } else {
                    // disableViews(false);
                    frameLoading.setVisibility(View.GONE);

                }
                //Remove dialog if showing
                FragmentTransaction fm = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().
                        findFragmentByTag("newAppDialog");
                if (prev != null) {
                    fm.remove(prev);
                    fm.commitAllowingStateLoss();
                }
            }
        });
        mainViewModel.getApplicationsOnProccess().observe(this, new Observer<List<CreditApplication>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onChanged(List<CreditApplication> creditApplications) {
                if (creditApplications != null) {
                   tvHeaderPendingApps.setText("Tiene "+creditApplications.size()+" solicitudes en proceso.");
                }
            }
        });



        // testing mainViewModel.getAllCreditApplications();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.log_out:
                showLogOutDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fabNewApplication)
    public void onFabNewApplicationClick() {
        //Show NewApplicationFragment
        FragmentTransaction fm = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().
                findFragmentByTag("newAppDialog");
        if (prev != null) {
            fm.remove(prev);
            fm.commitAllowingStateLoss();
        }
        NewApplicationDialogFragment dialogFragment = NewApplicationDialogFragment.newInstance();
        dialogFragment.show(fm, "newAppDialog");
    }


    /**
     * Shows Confirmation Logout dialog and sends to LoginActivity if logout is confirmed
     */
    private void showLogOutDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.log_out_dialog_title)
                .setMessage(R.string.log_out_dialog_message)
                .setPositiveButton(R.string.dialog_button_accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PreferencesHelper.getInstance().saveUsername(null);
                        Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(loginIntent);
                        finish();
                    }
                })
                .setNegativeButton(R.string.dialog_button_cancel, null)
                .setCancelable(false)
                .show();
    }


}
