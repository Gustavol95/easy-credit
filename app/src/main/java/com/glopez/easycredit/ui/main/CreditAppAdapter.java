package com.glopez.easycredit.ui.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.glopez.easycredit.R;
import com.glopez.easycredit.data.model.CreditApplication;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CreditAppAdapter extends RecyclerView.Adapter<CreditAppAdapter.ViewHolder> {

    private List<CreditApplication> creditApplications;
    private   DecimalFormat decimalFormatter;

    public CreditAppAdapter() {
        creditApplications = new ArrayList<>();
        decimalFormatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        decimalFormatter.applyPattern("#,###,###,##0.00");
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_credit_app, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(creditApplications.get(position));
    }

    @Override
    public int getItemCount() {
        return creditApplications.size();
    }

    public void setCreditApplications(List<CreditApplication> creditApplications) {
        this.creditApplications = creditApplications;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{


        @BindView(R.id.tvInitials)
        TextView tvInitials;
        @BindView(R.id.tvNumberOfInstallments)
        TextView tvNumberOfInstallments;
        @BindView(R.id.tvAmount)
        TextView tvAmount;
        @BindView(R.id.tvTotalAmount)
        TextView tvTotalAmount;
        @BindView(R.id.card)
        CardView card;
        CreditApplication creditApplication;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bind(CreditApplication creditApplication){
            this.creditApplication = creditApplication;
            tvAmount.setText("Monto $"+decimalFormatter.format(creditApplication.getAmount()));
            tvTotalAmount.setText("Total a pagar $"+decimalFormatter.format(creditApplication.getAmountWithInterest()));
            tvNumberOfInstallments.setText("No. Pagos: "+creditApplication.getNumberOfInstallments());

            switch (creditApplication.getStatus()){
                case CreditApplication.PENDING:
                    tvInitials.setBackgroundResource(R.drawable.circle_border_process);
                    tvInitials.setText("P");
                    break;
                case CreditApplication.ACCEPTED:
                    tvInitials.setBackgroundResource(R.drawable.circle_border_accepted);
                    tvInitials.setText("A");
                    break;
                case CreditApplication.REJECTED:
                    tvInitials.setBackgroundResource(R.drawable.circle_border_rejected);
                    tvInitials.setText("R");
                    break;
            }


        }
    }
}
