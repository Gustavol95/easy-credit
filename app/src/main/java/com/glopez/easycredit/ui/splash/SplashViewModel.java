package com.glopez.easycredit.ui.splash;

import android.os.Handler;

import com.glopez.easycredit.utils.Constants;
import com.glopez.easycredit.utils.PreferencesHelper;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

class SplashViewModel extends ViewModel {

    private MutableLiveData<Integer> startSplashObservable;

    /***
     * Validates if theres a user logged in
     */
    private void validateSession() {
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                boolean isLogged = PreferencesHelper.getInstance().getUserName() != null;

                if (isLogged)
                    startSplashObservable.postValue(Constants.LOGGED_IN);
                else
                    startSplashObservable.postValue(Constants.NO_SESSION);

            }
        };
        handler.postDelayed(r, 1000);
    }


    MutableLiveData<Integer> getStartSplashObservable() {
        if (startSplashObservable == null) {
            startSplashObservable = new MutableLiveData<>();
            //If first time
            validateSession();
            return startSplashObservable;
        } else
            return startSplashObservable;
    }
}
